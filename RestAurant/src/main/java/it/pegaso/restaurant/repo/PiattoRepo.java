package it.pegaso.restaurant.repo;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.pegaso.restaurant.model.Piatto;

@Repository
public interface PiattoRepo extends JpaRepository<Piatto, Integer> {


}
