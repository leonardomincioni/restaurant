package it.pegaso.restaurant.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.pegaso.restaurant.model.Ingrediente;

@Repository
public interface IngredienteRepo extends JpaRepository<Ingrediente, Integer> {

}
