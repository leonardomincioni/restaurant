package it.pegaso.restaurant.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import it.pegaso.restaurant.model.Ingrediente;
import it.pegaso.restaurant.repo.IngredienteRepo;

@Service
public class IngredienteService {

	@Autowired
	private IngredienteRepo ingredienteRepo;

	public List<Ingrediente> findAll() {
		return ingredienteRepo.findAll();
	}

	public Ingrediente findById(int id) {
		return ingredienteRepo.findById(id).get();
	}

	public Ingrediente save(Ingrediente i) {
		return ingredienteRepo.save(i);
	}

	public Ingrediente update(Ingrediente i) {
		Optional<Ingrediente> ing = ingredienteRepo.findById(i.getId());
		if (!ing.isPresent()) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "ingrediente non trovato");
		}
		return ingredienteRepo.save(i);
	}

	public void delete(int id) {
		Optional<Ingrediente> ing = ingredienteRepo.findById(id);
		if (!ing.isPresent()) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "ingrediente non trovato");
		}
		ingredienteRepo.deleteById(id);

	}
}
